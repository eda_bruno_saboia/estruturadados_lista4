package br.ufc.list4.q1

object Main {
  def main(args: Array[String]) {
    var hash: DynamicHashTable = new DynamicHashTable(2,100)
    
    hash.insertTable(8)
    hash.insertTable(1)
    hash.insertTable(7)
    hash.insertTable(14)
    hash.insertTable(22)
    hash.insertTable(33)
    hash.printTable()
    hash.findElement(8)
    hash.findElement(4)
    hash.removeElement(22)
    hash.findElement(22)
  }
}