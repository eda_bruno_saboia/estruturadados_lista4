package br.ufc.list4.q1

import scala.collection.mutable.ArrayBuffer

class DynamicHashTable(sizeBucket : Int, n : Int) extends IDynamicHashTable {
  
  var table = Array(new Bucket(sizeBucket, 1), new Bucket(sizeBucket,1))
  var globalRep: Int = 1
  
  private def hashfunction(value: Int) : Int = {
    value % Math.pow(2, globalRep).toInt
  }
  
  override def insertTable(value:Int) : Boolean = {
    var index = hashfunction(value)
    
    if (!table(index).endSizeBucket){
     table(index).insertElement(value)
     return true
    }else{
       if (globalRep == table(index).lR){
         globalRep +=1
         table ++= table
       } 
       
       table(index).lR+1
       var newBucket = new Bucket(sizeBucket,table(index).lR)
       var newIndex: Int = hashfunction(value)
       splitBucket(index, newIndex, newBucket)
       newBucket.insertElement(value)
       table(newIndex) = newBucket
    }
       
  false          
  }
  
  override def splitBucket(index: Int, newIndex: Int, newBucket : Bucket): Unit={
    table(index).bucket.find(i => hashfunction(i) == newIndex).map{
      case i => {
        newBucket.insertElement(i)
        table(index).removeElement(i)
      }
    }   
  }
  
  override def findElement(value : Int): Boolean ={
    var index = hashfunction(value)
    table(index).bucket.find( _ == value).map{
      case some => {
        println("O valor " + value + " foi encontrado no bucket: " +index)     
        return true
      }
    }
    println("O valor " + value + " nao foi encontrado")
    return false
  }
  
  override def removeElement(value: Int) : Boolean ={
    var index = hashfunction(value)
    table(index).bucket.find( _ == value).map{
      case some => {
        table(index).removeElement(value)
        if (table(index).bucket.size == 0){
          println("entrou aqui")
          table(index) == Nil
        }
        println("O valor " + value + " foi removido do bucket: " +index)     
        return true
      }
    }
    println("O valor " + value + " nao foi encontrado")
    return false 
  }
  
  override def freeHash(): Unit ={
    table = null
  }
  
  override def printTable(): Unit ={
    for (i <- 0 until table.length){
      println(table(i).printBucket())
    }
  }
  
  
}