package br.ufc.list4.q1

import scala.collection.mutable.ListBuffer

trait IDynamicHashTable {
  
  def insertTable(n : Int) : Boolean
  def splitBucket(n1 : Int, n2 : Int, n3 : Bucket)
  def findElement(value : Int) : Boolean
  def removeElement(n : Int) : Boolean
  def freeHash()
  def printTable()
}