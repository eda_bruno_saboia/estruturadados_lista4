package br.ufc.list4.q1

import scala.collection.mutable.ArrayBuffer

class Bucket (bucketSize: Int, localRep: Int) {
  var bucket : ArrayBuffer[Int] = new ArrayBuffer[Int](bucketSize)
  var lR = localRep
    
  def endSizeBucket : Boolean = bucket.size == bucketSize
  
  def insertElement(value : Int): Boolean ={
    if (!endSizeBucket){
      bucket += value
      return true
    }
    false
  }
  
  def removeElement (value : Int): Boolean = {
    if (bucket != null){
      bucket -= value
      return true
    }
    false
  } 
  
  def printBucket () : Unit ={
    for (i <- 0 until(bucket.length))
      println(bucket(i))
    
  }
  
}